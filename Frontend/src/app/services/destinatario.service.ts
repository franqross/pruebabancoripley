import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable} from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Destinatario } from '../models/destinatario';
import { Banco } from '../models/banco';

const URL = 'http://localhost:4000/api/banco/destinatarios';
//const URL = '1';


@Injectable({
  providedIn: 'root'
})
export class DestinatarioService {


  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  }
  constructor(private http: HttpClient) 
  {
  }

  crearDestinatario(destinatario: Destinatario): Observable<any>{
    return this.http.post<Destinatario>(URL+"/crear", destinatario);
  }
    

  buscarDestinatario(rut: any): 
  Observable<Destinatario[]> {
    console.log("servicio"+1);
    return this.http.get<Destinatario[]>(`${URL}/rut/1`);
  }

  listadoBanco(): Observable<Banco[]> {
    const link = "https://bast.dev/api/banks.php";
    return this.http.get<Banco[]>(link);
  }
}
