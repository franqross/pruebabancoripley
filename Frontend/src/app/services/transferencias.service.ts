import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Transferencia } from '../models/transferencia';

const URL = 'http://localhost:4000/api/banco/transferencias/';



@Injectable({
  providedIn: 'root'
})
export class TransferenciasService {

  constructor(private http: HttpClient) { }
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Credentials': 'true'
    })
  }

  crearTransferencia(transferencia: Transferencia): Observable<any>{
    return this.http.post<Transferencia>(URL+"hacer", transferencia);
  }

  historiaTransferencias(): Observable<Transferencia[]> {
    return this.http.get<Transferencia[]>(URL+"listado");
  }
}
