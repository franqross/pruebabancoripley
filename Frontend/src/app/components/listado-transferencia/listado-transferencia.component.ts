import { Component, OnInit } from '@angular/core';
import { Transferencia } from 'src/app/models/transferencia';
import { TransferenciasService } from 'src/app/services/transferencias.service';

@Component({
  selector: 'app-listado-transferencia',
  templateUrl: './listado-transferencia.component.html',
  styleUrls: ['./listado-transferencia.component.css']
})
export class ListadoTransferenciaComponent implements OnInit {

  transferencias?: Transferencia[];

  constructor(private transferenciasService: TransferenciasService) { }


  ngOnInit(): void {
    this.listadoTransferencias();
  }

  listadoTransferencias(): void{
    this.transferenciasService.historiaTransferencias()
      .subscribe(
        data => {
          this.transferencias = data;
          console.log(data);
        },
        error => {
          console.log(error);
        });
  }

}
