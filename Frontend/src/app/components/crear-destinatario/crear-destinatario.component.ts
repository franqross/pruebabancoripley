import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Banco } from 'src/app/models/banco';
import { Destinatario } from 'src/app/models/destinatario';
import { DestinatarioService } from 'src/app/services/destinatario.service';

@Component({
  selector: 'app-crear-destinatario',
  templateUrl: './crear-destinatario.component.html',
  styleUrls: ['./crear-destinatario.component.css']
})
export class CrearDestinatarioComponent implements OnInit {

  destinatario: Destinatario = {
    rut:'',
    nombre:'',
    correo:'', 
    numTelefono:'', 
    banco:'', 
    tipoCuenta:'', 
    numeroCuenta:''
  };
  submitted = false;

  formDestinatario:FormGroup;

  bancos : Banco[ ] = [];
  bancos2 = [
    {
      "banks": [
        {
          "name": "Banco Ripley",
          "id": "0000001"
        },
        {
            "name": "Banco Desarrollo",
            "id": "0000002"
        },
        {
            "name": "Banco Estado",
            "id": "0000003"
        },
        {
            "name": "Banco Chile",
            "id": "0000004"
        },
        {
            "name": "Banco Santander",
            "id": "0000006"
        },
        {
            "name": "Banco Edwards",
            "id": "0000007"
        },
        {
            "name": "Banco BCIbe",
            "id": "0000008"
        }
      ]
    }
  ]


  constructor(private formBuilder: FormBuilder,private destinatarioService: DestinatarioService) {
    this.formDestinatario = this.formBuilder.group({
      'rut' : [null, Validators.required],
      'nombre': [null, Validators.required],
      'correo': [null, Validators.required],
      'numTelefono': [null, Validators.required],
      'banco': [null, Validators.required],
      'tipoCuenta': [null, Validators.required],
      'numeroCuenta': [null, Validators.required],
    })
   }

  ngOnInit(): void {
    this.listadoBancos();
  }

  crearDestinatario(){
    const desti = {
      rut: this.destinatario.rut,
      nombre: this.destinatario.nombre,
      correo: this.destinatario.correo, 
      numTelefono: this.destinatario.numTelefono, 
      banco: this.destinatario.banco, 
      tipoCuenta: this.destinatario.tipoCuenta, 
      numeroCuenta:this.destinatario.numeroCuenta
    }
    console.log(desti);
    this.destinatarioService.crearDestinatario(desti).subscribe(res => {
      console.log(res);
      //console.log('Post created successfully!');
    })
  }

  listadoBancos(): void{
    this.destinatarioService.listadoBanco()
      .subscribe(
        data => {
          this.bancos = data;
          console.log(this.bancos);
        },
        error => {
          console.log(error);
        });
  }
  

}
