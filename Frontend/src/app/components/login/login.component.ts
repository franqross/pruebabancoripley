import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { Usuario } from 'src/app/models/usuario';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: Usuario = {
    username: '',
    password: '',
  };
  submitted = false;

  formLogin: FormGroup;

  title = "Banco Ripley";
  logins = "Ingrese sus Datos";


  constructor( private router: Router,formBuilder: FormBuilder) 
  {
    this.formLogin = formBuilder.group({
      username: ['',Validators.required],
      password: ['',Validators.required]
    });
  }

  ngOnInit(): void {
  }

  login()  
  {
    const login: Usuario = {
      username: this.usuario.username,
      password: this.usuario.password
    };
    /*const usuario: Usuario = {
      username: this.formLogin.value.username,
      password: this.formLogin.value.password,
    };*/
    console.log(login);
    if(login.username=="123" && login.password=="123")
    {
      this.router.navigate(['/home']);
    }else{
      this.router.navigate(['/']);
    }
  }

}
