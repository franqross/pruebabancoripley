import { Component, OnInit } from '@angular/core';
import { Destinatario } from 'src/app/models/destinatario';
import { Transferencia } from 'src/app/models/transferencia';
import { DestinatarioService } from 'src/app/services/destinatario.service';
import { TransferenciasService } from 'src/app/services/transferencias.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-buscar-destinatario',
  templateUrl: './buscar-destinatario.component.html',
  styleUrls: ['./buscar-destinatario.component.css']
})
export class BuscarDestinatarioComponent implements OnInit {

  accesso = false;
  destinatario = Destinatario;
  transferencia: Transferencia = {
    monto: 0,
    destinatario: Object({_id: 0,})
  }
  rut = "";
  destinatarioSeleccionado: Destinatario[] =[];
  monto=0;
  id = 0;
  title = "Detalle del Destinatario";

  constructor(private destinatarioService: DestinatarioService, private transferenciasService: TransferenciasService) { }
  


  ngOnInit(): void {
  }

  searchTitle = '';

  buscarDestinatario(): void{
    this.destinatarioService.buscarDestinatario(this.rut)
    .subscribe(
      data  => {
        this.destinatarioSeleccionado.pop();
        this.destinatarioSeleccionado.push(data);
      },
      error => {
        console.log(error);
      });
  }

  miFuncion(variable:string):void{
    console.log("la variable que ingreso en nuestra funcion es :" + variable);
  }

  miFuncionConDevolucion():string{
      return "Que bueno saber que tipo va a devolver";
  }

  hacerTransferencia(){



    this.destinatarioService.buscarDestinatario(this.rut)
    .subscribe(
      data  => {
        this.title="Detalle Destinatario"
        this.destinatarioSeleccionado.pop();
        this.destinatarioSeleccionado.push(data);
        this.id = Object(data._id);
        console.log(this.id);
      },
      error => {
        console.log(error);
      });


    const trans = {
      monto: this.monto,
      destinatario:  Object(this.id),      
    }
    console.log("id ",this.id);
    this.transferenciasService.crearTransferencia(trans).subscribe(res => {
      

      console.log("Transferido");
      Swal.fire({
        title: 'La Transferencia!',
        text: 'Ha sido hecho con Exito',
        icon: 'success',
        confirmButtonText: 'Cool'
      })
})

    
  }

  

}
