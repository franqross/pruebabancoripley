import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  title = 'Bienvenido a Banco Ripley';

  constructor( private router: Router) { }

  ngOnInit(): void {
  }

  logout(){
    console.log("Session Cerrada");
    this.router.navigate(['/login']);
  }

}
