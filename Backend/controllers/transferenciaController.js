const Transferencia = require('../models/Transferencia');

exports.hacerTransferencia = async (req,res) => {

    try{
        let transferencia;

        transferencia = new Transferencia(req.body);

        await transferencia.save();
        
        res.send(transferencia);

    }catch(error)
    {
        console.log(error);
        res.status(500).send('Hubo un error');

    }
}

exports.historialTransferencias = async (req, res) => {
    try
    {
        const transferencias = await Transferencia.find();
        res.json(transferencias);

    }catch(error){
        console.log(error);
        res.status(500).send('Hubo un error');
    }
}