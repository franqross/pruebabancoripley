const Usuario = require('../models/Usuario');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

exports.registrar = async (req,res) =>{

    let body = req.body;

    let { username, password } = body;
    let usuario = new Usuario({
        username,
        password: bcrypt.hashSync(password, 10),
    });
    usuario.save((err, usuarioDB) => {
        if (err) {
        return res.status(400).json({
            ok: false,
            err,
        });
        }
        res.json({
            ok: true,
            usuario: usuarioDB
        });
        })

}

exports.login = async (req,res) => {
    try
    {
        let body = req.body;


        let usuario = await Usuario.findOne({username : body.username});

        if (!usuario) {
            return res.status(400).json({
              ok: false,
              err: {
                  message: "Usuario incorrecto"
              }
           })
         }

        let passwordIsValid = bcrypt.compareSync(
                req.body.password,
                usuario.password
        );
        
        if(!passwordIsValid){
            return res.status(401).send({
                accessToken: null,
                message: "Password Invalido"
              });
        }

        /*let token = jwt.sign({
            usuario,
         },SEED_AUTENTICACION, {
         expiresIn: CADUCIDAD_TOKEN
        })*/

        let token = "2132565653242635";
         
         res.json({
            ok: true,
            usuario,
            token,
        })
        

         /*

        Usuario.findOne({ username: body.username }, (erro, usuarioDB)=>{
            if (erro) {
              return res.status(500).json({
                 ok: false,
                 err: erro
              })
           }
       // Verifica que exista un usuario con el mail escrita por el usuario.
          if (!usuarioDB) {
             return res.status(400).json({
               ok: false,
               err: {
                   message: "Usuario o contraseña incorrectos"
               }
            })
          }
       // Valida que la contraseña escrita por el usuario, sea la almacenada en la db
          if (! bcrypt.compareSync(body.password, usuarioDB.password)){
             return res.status(400).json({
                ok: false,
                err: {
                  message: "Usuario o contraseña incorrectos"
                }
             });
          }
       // Genera el token de autenticación
           let token = jwt.sign({
                  usuario: usuarioDB,
               }, process.env.SEED_AUTENTICACION, {
               expiresIn: process.env.CADUCIDAD_TOKEN
           })
           res.json({
               ok: true,
               usuario: usuarioDB,
               token,
           })
       })
       */

    }catch(error)
    {
        console.log(error);
        res.status(500).send('Hubo un error');
    }
}