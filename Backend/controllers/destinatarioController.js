const Destinatario = require('../models/Destinatario');

exports.obtenerDestinatarios = async (req, res) => {
    try
    {
        const destinatarios = await Destinatario.find();
        res.json(destinatarios);

    }catch(error){
        console.log(error);
        res.status(500).send('Hubo un error');
    }
}

exports.crearNuevoDestinatario = async (req,res) => {
    try{
        let destinatario;

        destinatario = new Destinatario(req.body);  
        
        /*let respuesta;

        respuesta = validarExistenciaRut(destinatario.rut);

        console.log("Repuesta: "+respuesta);
        
        if(!respuesta)
        {
            console.log("Aprobado");*/
            await destinatario.save();
        /*}else{
            console.log("Vete a la mierda");
        }
        console.log("Probando");*/

                
        res.send(destinatario);

    }catch(error)
    {
        console.log(error);
        res.status(500).send('Hubo un error');

    }
}

exports.obtenerRut = async (req,res) =>{

    try
    {
        let destinatarios = await Destinatario.findOne({'rut':  req.params.rut} );
        if(!destinatarios){
            res.status(400).json({msg: 'No existe el Destinatario'});
        }

        res.json(destinatarios);

    }catch(error){
        console.log(error);
        res.status(500).send('Hubo un error');
    }
    
}
