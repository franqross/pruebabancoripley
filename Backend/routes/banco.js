const express = require('express');
const router = express.Router();
const destinatarioController = require('../controllers/destinatarioController');
const transferenciaController = require('../controllers/transferenciaController');
const usuarioController = require('../controllers/usuarioController');
require("dotenv").config();


router.post('/usuarios/login', usuarioController.login);
router.post('/usuarios/registrar', usuarioController.registrar);

router.get('/destinatarios/listado',destinatarioController.obtenerDestinatarios);
router.post('/destinatarios/crear',destinatarioController.crearNuevoDestinatario);
router.get('/destinatarios/rut/:rut',destinatarioController.obtenerRut);

router.post('/transferencias/hacer',transferenciaController.hacerTransferencia);
router.get('/transferencias/listado',transferenciaController.historialTransferencias);


module.exports = router;
