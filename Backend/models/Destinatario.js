const mongoose = require('mongoose');

const DestinatarioSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    rut: {
        type: String,
        required: true
    },
    correo: {
        type: String,
        required: true
    },
    numTelefono: {
        type: String,
        required: true
    },
    banco: {
        type: String,
        required: true
    },
    tipoCuenta: {
        type: String,
        required: true
    },
    numeroCuenta: {
        type: String,
        required: true
    },
});

module.exports = mongoose.model('Destinatario',DestinatarioSchema);